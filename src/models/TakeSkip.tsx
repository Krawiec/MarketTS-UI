class TakeSkip{
    public take:number;
    public skip:number;
    
    public constructor(take: number, skip:number) { 
        this.take = take; 
        this.skip = skip;
    }

}


export { TakeSkip };